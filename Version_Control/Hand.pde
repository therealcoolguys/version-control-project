class Hand {
  
  float handLoc;
  color palm = color(18,25,14);
  color fingers = color(21, 36, 17);

    void display() {
    handLoc = mouseX/2;
    noStroke();
    fill(palm); //set fill for palm and thumb, darker green
    ellipseMode(CORNER);
    ellipse(handLoc, 290, 160, 65);//palm
    ellipse(handLoc-25, 305, 20, 20);//thumb tip
    ellipse(handLoc-20, 310, 40, 20);//thumb
    fill(fingers); //change fill for fingers, lighter green
    ellipse(handLoc, 260, 30, 80);//left
    ellipse(handLoc+60, 260, 30, 80);//middle
    ellipse(handLoc+120, 260, 30, 80);//right
    ellipse(handLoc+8, 310, 30, 30);//base of left finger
    ellipse(handLoc+112, 310, 30, 30);//base of right finger
    fill(96, 118, 84);//fingernail color
    ellipse(handLoc+8, 265, 13, 8);
    ellipse(handLoc+68, 265, 13, 8);
    ellipse(handLoc+128, 265, 13, 8);
  }
}
