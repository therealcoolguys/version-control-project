class Rain {
  float x = random(width);
  float y = random(-500, -50);
  float yspeed = random(5, 10);
  float dropsize = random(10, 20);
  float gravity = 0.1;
  
  void fall () {
    y = y + yspeed;
    yspeed = yspeed + gravity;
    
    if (y>height) {
      y = random(-200, -100);
      yspeed = random(5, 10);
    }
  }
  
  void display() {
    
    stroke(random(90, 100), 0, 0);
    line(x, y, x, y+dropsize);
  }
}
