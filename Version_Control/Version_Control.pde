//VERSION CONTROL ASSIGNMENT//
//GROUP MEMBERS:
//GRAHAM LEE
//HONGMING WANG
//ALEXANDER KEHN

//PSEUDOCODE
//draw blood rain in background
//draw an array of orbs
//orbs follow after the mouse
//the orbs' accelerate more the closer they get to the mouse location
//draw a big wizard dude with glowing eyes
//draw wizards' hand and make it follow mouseX

Orb[] orb = new Orb[5]; //an array of the bigger orb, was a singular but looks nicer as an array
Orbs[] orbs = new Orbs[20]; //an array of the smaller orbs

Rain[] rain = new Rain[300]; //an array of rain drops

Hand hand = new Hand();
Wizard wizard = new Wizard();
void setup() {
  frameRate(60);
  size(400, 400);
  
  for (int i = 0; i < rain.length; i++) {
    rain[i] = new Rain();
  }

  for (int i = 0; i < orb.length; i++) {
    orb[i] = new Orb();
  }
    for (int i = 0; i < orbs.length; i++) {
    orbs[i] = new Orbs();
  }
  //for loops to initialize the rain and orbs will go here
}


void draw(){
background(20);
noCursor();





  //forloop to call the falling rain will go here as such:
  for (int i = 0; i<rain.length; i++) {
      rain[i].fall();
      rain[i].display();
      //each iteration calls fall and display, which creates the effect
      //see Rain class for details
  }
  wizard.display();
  for (int i = 0; i < orbs.length; i++) {
    orbs[i].update();
    orbs[i].display();
  }
    for (int i = 0; i < orb.length; i++) {
    orb[i].update();
    orb[i].display();
  }

hand.display();
}
