class Orb {

  PVector location;
  PVector velocity;
  PVector acceleration;
  float orbit;
  color OrbColor;


  Orb() {
    location = new PVector(random(width), random(height));
    velocity = new PVector(0, 0);
    orbit = 4; //the limit on velocity "orbit size" lower == tighter
    setOrbColor();
  }

  void update() {

    //find the vector for mouse movement
    PVector mouse = new PVector(mouseX, mouseY);
    PVector dir = PVector.sub(mouse, location); //orb location subtracts mouse
    dir.normalize(); //set this value to 1
    dir.mult(0.2); //scale the direction, lower == looser orbit
    acceleration = dir; //assign all this to acceleration

    //acceleration changes velocity,  location changes by velocity.
    velocity.add(acceleration);
    velocity.limit(orbit);
    location.add(velocity);
  }

  void setOrbColor() {
    OrbColor= color(0, random(245, 255), random(220, 231));
  }

  // display the Orb
  void display() {
    ellipseMode(CENTER); //the code for the hand screwed up the orbs so ensure they are reset to center
    //glow width flickers in size
    float gW = random(10, 12);
    //for loop to create the glow, color alpha and glow width are affected by the loop
    for (int i=0; i<10; i++) {
      fill(OrbColor, 25-i);
      ellipse(location.x, location.y, gW+(i*20), gW+(i*20));
    }

    //orb is a big dumb circle
    noStroke();
    fill(OrbColor);
    ellipse(location.x, location.y, 80, 80);
  }
}
