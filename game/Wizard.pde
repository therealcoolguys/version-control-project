class Wizard {


  void display() {
    fill(0);
    noStroke();
    triangle(0, 400, 340, 20, 602, 410);//gigantic cloak
    fill(39, 61, 13);
    ellipseMode(CENTER);
    ellipse(336, 121, 80, 100);  //face

    //this re-used mouth is insanely messy...you should not be lazy and rewrite
    //it into a clean object that can have it's x/y adjusted easily
    //MOUTH
    fill(30, 41, 22); //dark green
    ellipse(324, 128, 20, 20); //left cheek
    ellipse(348, 128, 15, 15); //right cheek
    ellipse(335, 131, 36, 20); //this circle helps form the smile (lower lip)
    fill(39, 61, 13); //change back to torso color
    ellipse(337, 117, 18, 16); //this circle helps form the smile (upper lip)
    //TEETH
    fill(230, 241, 180); //light green
    noStroke();
    rect(344, 134, 5, 5, 2);
    rect(321, 133, 5, 5, 2);

    //EYES
    float gW = random(10, 17);
    //for loops create the glowing eyes, gW = glow width
    for (int i=0; i<9; i++) {
      fill(255, 117, 0, 25-i);
      ellipse(320, 96, gW+(i*3), gW+(i*2));
    }

    for (int i=0; i<9; i++) {
      fill(255, 117, 0, 25-i);
      ellipse(354, 99, gW+(i*3), gW+(i*2));
    }

    fill(255, 117, 0); //orange eyes
    ellipse(320, 100, 20, 20);
    ellipse(354, 103, 20, 20);
    fill(39, 61, 13); //reset to face color, two ellipse create the moon shape
    ellipse(320, 104, 20, 20);
    ellipse(354, 107, 20, 20);
  }
}
