class Orbs {

  PVector location;
  PVector velocity;
  PVector acceleration;
  float orbit;
  color orbsColor;


  Orbs() {
    location = new PVector(random(width), random(height));
    velocity = new PVector(0, 0);
    orbit = 4; //the limit on velocity "orbit size" lower == tighter
    setOrbsColor();
  }

  void update() {

    //find the vector for mouse movement
    PVector mouse = new PVector(mouseX, mouseY);
    PVector dir = PVector.sub(mouse, location); //orb location subtracts mouse
    dir.normalize(); //set this value to 1
    dir.mult(0.1); //scale the direction, lower == looser orbit
    acceleration = dir; // set to acceleration.

    //acceleration changes velocity,  location changes by velocity.
    velocity.add(acceleration);
    velocity.limit(orbit);
    location.add(velocity);
  }

  void setOrbsColor() {
    orbsColor= color(random(0, 50), random(200, 255), random(200, 255));
  }

  // display the Orbs
  void display() {
    ellipseMode(CENTER);//the code for the hand screwed up the orbs so ensure they are reset to center
    //glow
    float gW = random(10, 12);

    for (int i=0; i<10; i++) {
      fill(orbsColor, 25-i);
      ellipse(location.x, location.y, gW+(i*8), gW+(i*8));
    }
    //orbs
    noStroke();
    fill(orbsColor);
    ellipse(location.x, location.y, 30 + (sin(frameCount * 0.05)*10), 30 + sin(frameCount * 0.05)*10); //the ol' sinFrameCount trick changes their size. could probably be achieved more effectively with a vector
  }
}
