//------------------PROG ASSIGNMENT 3-----------------//
//-------------------ALEXANDER KEHN-------------------//
//-------------------id# 991368989--------------------//
//------"Hands of Most Ultimate Doom w/ Blood Rain"---//
//---------------------OR-----------------------------//
//---"How I Said I'd Make Something Super Polished----//
//-------and Got Lazy and Ran Out of Time Again"------//

//PSEUDOCODE
//draw blood rain in background
//draw an array of orbs
//orbs follow after the mouse
//the orbs' accelerate more the closer they get to the mouse location
//draw a big wizard dude with glowing eyes
//draw wizards' hand and make it follow mouseX


Orbs[] orbs = new Orbs[20]; //an array of the smaller orbs
Orb[] orb = new Orb[5]; //an array of the bigger orb, was a singular but looks nicer as an array
Hand hand = new Hand(); //load in the hand and wizard class
Wizard wizard = new Wizard();
Rain[] rain = new Rain[300]; //an array of rain drops

void setup() {
  frameRate(60);
  size(400, 400);

  //for loops to initialize the rain and orbs
  for (int i = 0; i < rain.length; i++) {
    rain[i] = new Rain();
  }

  for (int i = 0; i < orb.length; i++) {
    orb[i] = new Orb();
  }

  for (int i = 0; i < orbs.length; i++) {
    orbs[i] = new Orbs();
  }
}

void draw() {
  background(20); //background is very dark for effect
  noCursor();
  //for the length of the rain array, call upon the fall and display functions
  for (int i = 0; i<rain.length; i++) {
    rain[i].fall();
    rain[i].display();
    //each iteration calls fall and display, which creates the effect
    //see Rain class for details
  }

  wizard.display();

  //similar for loops to display the orbs
  for (int i = 0; i < orbs.length; i++) {
    //call functions for all the objects in the array
    orbs[i].update();
    orbs[i].display();
  }

  for (int i = 0; i < orb.length; i++) {
    orb[i].update();
    orb[i].display();
  }

  hand.display();
}
